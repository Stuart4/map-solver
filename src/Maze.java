/**
 * Created by jake on 7/29/14.
 */
import java.util.Arrays;
public class Maze {

    char[][] mazeMap;
    char[][] solution;
    int xDim, yDim;
    int xHead, yHead;
    node[][] mazeNodes;//TODO remove from class variables
    node head;

    Maze(String[] rows) {

        mazeMap = new char[rows.length][rows[0].length()];

        for (int i = 0; i < rows.length; i++) {
            mazeMap[i] = rows[i].toCharArray();
            System.out.println(rows[i].toCharArray());
        }

        buildObMaze();


    }

    public void printSolution() {

        String toPrint = "";

        boolean solved = solve(head);


        for (char[] line : mazeMap) {

            for (char space : line) {
                toPrint += space;

            }
            toPrint += "\n";
        }

        System.out.println(solved);
        System.out.println(toPrint);

    }

    private boolean solve (node currentNode){
        System.out.println(currentNode);
        mazeMap[currentNode.y][currentNode.x] = '*';
        if(currentNode.isFinish) return true;
        System.out.println("Solving");

        currentNode.visited = true;

        if(currentNode.right != null && currentNode.right != currentNode.previous && !currentNode.right.visited){
            currentNode.right.previous = currentNode;
            return solve(currentNode.right);
        } else if (currentNode.down != null && currentNode.down != currentNode.previous && !currentNode.down.visited){
            currentNode.down.previous = currentNode;
            return solve(currentNode.down);
        }else if (currentNode.left != null && currentNode.left != currentNode.previous && !currentNode.left.visited){
            currentNode.left.previous = currentNode;
            return solve(currentNode.left);
        }else if (currentNode.up != null && currentNode.up != currentNode.previous && !currentNode.up.visited){
            currentNode.up.previous = currentNode;
            return solve(currentNode.up);
        } else {
            mazeMap[currentNode.y][currentNode.x] = ' ';
            return solve(currentNode.previous);
        }


    }

//    private char[][] solve(node currentNode, node previous, char[][] path, int xPos, int yPos) {
//        path[yPos][xPos] = '*';
//
//        if (currentNode.isFinish == true) {
//            System.out.println("finished");
//            return path;
//        }
//
//
//        if (currentNode.left != null && currentNode.left != currentNode.previous) {
//            currentNode.down.previous = currentNode;
//            char[][] sol = solve(currentNode.left,currentNode, path, --xPos, yPos);
//            if (sol != null) return sol;
//        }
//
//        else if (currentNode.right != null && currentNode.right != currentNode.previous) {
//            currentNode.down.previous = currentNode;
//            char[][] sol = solve(currentNode.right, currentNode, path, ++xPos, yPos);
//            if (sol != null) return sol;
//        }
//        else if (currentNode.up != null && currentNode.up != currentNode.previous){
//            currentNode.down.previous = currentNode;
//            char[][] sol = solve(currentNode.up, currentNode, path, xPos, --yPos);
//            if (sol != null) return sol;
//        }
//
//        else if (currentNode.down != null && currentNode.down != currentNode.previous) {
//            currentNode.down.previous = currentNode;
//            char[][] sol = solve(currentNode.down, currentNode, path, xPos, ++yPos);
//            if (sol != null) return sol;
//
//        }
//        path[yPos][xPos] = ' ';
//        return solve(currentNode.previous, currentNode.previous.previous, path, currentNode.previous.x,currentNode.previous.y);
//    }

    private boolean checkIfSolution(int row, int column) {
        if (mazeMap[row][column] == ' ') {
            if (row == mazeMap.length - 1 || column == 0 || column == mazeMap[0].length - 1) return true;
        }
        return false;
    }


    private void buildObMaze() {
        mazeNodes = new node[mazeMap.length][mazeMap[0].length];

        for (int r = 0; r < mazeNodes.length; r++) {

            for (int c = 0; c < mazeNodes[0].length; c++) {

                if (mazeMap[r][c] == ' ') {
                    node currentNode = new node();
                    currentNode.x = c;
                    currentNode.y = r;
                    if (checkIfSolution(r, c)) {
                        currentNode.isFinish = true;
                        System.out.println("found finish");
                    }

                    //left
                    if (c > 0 && mazeNodes[r][c - 1] != null) {
                        currentNode.left = mazeNodes[r][c - 1];
                        mazeNodes[r][c - 1].right = currentNode;
                    }

                    //up

                    if (r > 0 && mazeNodes[r - 1][c] != null) {
                        currentNode.up = mazeNodes[r - 1][c];
                        mazeNodes[r - 1][c].down = currentNode;
                    }

                    mazeNodes[r][c] = currentNode;

                    if (head == null) {
                        head = currentNode;

                        xHead = r + 1;
                        yHead = c - 1;
                    }


                }

            }
        }
    }
}

  class node {
     node left;
     node right;
     node up;
     node down;
     node previous;

      int x, y;

     boolean isFinish = false;
     boolean visited ;

 }


