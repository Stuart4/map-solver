/**
 * Created by jake on 7/29/14.
 */
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
public class Main {

    public static void main(String[] args) {

        File mazeFile = new File(args[0]);
        int lineCount = 0;
        LinkedList<String> mazeLines = new LinkedList<String>();

        BufferedReader br;

        try {

            String line;

            br = new BufferedReader(new FileReader(args[0]));

            while ((line = br.readLine()) != null){
                mazeLines.add(line);
                lineCount++;
            }
            br.close();
        } catch (IOException e){
            e.printStackTrace();

        }
        String[] mazeArray = new String[lineCount - 1];

        Maze maze = new Maze(mazeLines.toArray(mazeArray));

        maze.printSolution();

    }
}

